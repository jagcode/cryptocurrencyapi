﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPISample.Models;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace WebAPISample.Controllers
{
    public class StocksController : ApiController
    {
        Stock[] stocks = new Stock[]
        {
            //sample dataset
            new Stock { Id = 1, Name = "Bitcoin", Code = "BTC"},
            new Stock { Id = 2, Name = "Litecoin", Code = "LTC" },
            new Stock { Id = 3, Name = "Ethereum", Code = "ETH" }
        };

        public IEnumerable<Stock> GetAllStocks()
        {
            // can call database to list all stocks
            return stocks;
        }

        public IHttpActionResult GetStock(string id )
        {
            var stock = stocks.FirstOrDefault((p) => p.Code == id.ToUpper());
            if (stock == null)
            {
                return NotFound();
            }
            
            //service call to cryptocurrency api
            var client = new HttpClient();
            Uri target = new Uri("https://min-api.cryptocompare.com/data/price?tsyms=USD&fsym=" + id.ToUpper());
            var response = client.GetAsync(target).Result;
            if (response.IsSuccessStatusCode)
            {
                var modelObject = response.Content.ReadAsAsync<Stock>().Result;;
                stock.USD = modelObject.USD;
            }
            return Ok(stock);
        }

        
    }
}
